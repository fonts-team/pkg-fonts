#!/bin/bash

# bash script for updating Standards-Version to 3.9.2, d/compat to 8 and debhelper to (>= 8)
# It supposes old package has Standards-Version = 3.8.4
# Before run it, you need to clone all perl packages present on git using 'mr up' (see 
# http://pkg-perl.alioth.debian.org/git.html#track_all_repositories_on_alioth_with_mr_1_ for details) 
# Lunch it from ~/src/pkg-perl/git/packages directory


# Create list of package with 3.8.4 Standards-Version
grep -r 3.8.4 */debian/control |cut -f1 -d'/' > list

# Disable temporarily KGB commit messages 
ssh vasks.debian.org 'touch /home/groups/pkg-perl/KGB-notifications-disabled'

for i in (cat list)
do
   cd $i/debian
   sed -i s'/3.8.4/3.9.2/' control
   sed -i '/^Build-Depends/s/debhelper (>= [0-9].*)/debhelper (>= 8)/' control
   echo 8 > compat
   dch -a "Update to 3.9.2 Standards-Version (no changes)."
   dch -a "Update d/compat to 8"
   dch -a "Update debhelper to (>= 8)"
   cd ..
   git add .
   git commit -m "*Update to 3.9.2 Standards-Version (no changes). *Update d/compat to 8 *Update debhelper to (>= 8)"
   git push origin master
   cd ..
done

# Re-enable KGB commit messages
ssh vasks.debian.org 'rm /home/groups/pkg-perl/KGB-notifications-disabled'

# Remove temporarily list package previously created.
rm -f list
